<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>


<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="slide_lux/engine1/style.css" />
<script type="text/javascript" src="slide_lux/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
</head>

<body>
	<div class="content_villa kozu">
    	<div class="menu_and_text">
        	<div class="logo_text">
            	<span style="color:#666">LUXURY</span> VILLA
            </div>
            
        	<div class="menu_villa">
            	<div class="inline_menu_villa">ABOUT</div>
                <div class="inline_menu_villa">BOOK NOW</div>
                <div class="inline_menu_villa">BLOG</div>
                <div class="inline_menu_villa">AREAS</div>
                <div class="inline_menu_villa">GALLARY</div>
                <div class="inline_menu_villa">FIND US</div>
            </div>
        </div>
        
        <div class="slide_left_and_bloc_text">
            <div class="navi_left">
            	<div class="sub_menu">
                    <div class="menu_div">home</div>
                    <div class="menu_div">about us</div>
                    <div class="menu_div">gallary</div>
                    <div class="menu_div">contact</div>
                </div>
                <div class="para">
                	<div class="title_lu">
                    	<span style="font-size:30px; font-weight:bold">Luxury</span><br />
                        <span style="font-size:30px">Villa</span>
                    </div>
                    <div class="para_detail">
                    	We love that, humanism is the soul of design and enables transformative results. We view each design opportunity as an unfolding story – and the person occupying the space or using a product – as the central character. By focusing on the individual, we humanize design. As a result, people experience our work spatially, visually and emotionally.<br /><br />

We care that, architecture is inextricably tied to use, place, client aspirations and cultural traditions.  We also believe that the greatest buildings are those that cannot be imagined anywhere else in the world.  Thus, it is our mission to provide clients around the globe with highly functional, intuitive design solutions that are sensitive to an ever changing built environment in both scale and context.  We bring design excellence to all building types.<br /><br />

We share that, interior design should reflect each client’s culture and communicate their values while meeting both their functional and aesthetic goals. Our projects range from company headquarters to international hotels to interiors for public.

                    </div>
                </div>	
            </div>
            <div class="slide_right">
            	<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
<div class="ws_images"><ul>
		<li><img src="images/05.jpg" alt="Desert" title="Desert" id="wows1_0"/></li>
	</ul></div>
</div>	
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->
            </div>
        </div>
    </div>
</body>
</html>