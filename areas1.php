<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
<title>Alpha Development / Phnom Penh Kingdom of Cambodia.</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" href="trajan_pro/fonts.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<link rel="stylesheet" type="text/css" href="slide_small/engine2/style2.css" />
<link rel="stylesheet" type="text/css" href="css/stylesmallslide.css" />
<link rel="stylesheet" type="text/css" href="fonts/TrajanPro-Regular/fonts.css" />


<script src="js/jquery-1.8.3.js"></script>
<script src="js/script.js" type="text/javascript"></script>
<script type="text/javascript" src="js/flowslider.jquery.js"></script>
<script>
	jQuery(document).ready(function($) {
		$("#slider").FlowSlider({
			
			marginStart: 1,
			marginEnd: 0,
			infinite: false,
			
			animationOptions: {
				snap: true
			},
			position: 0.0,
			controllers: ["Event", "Event"],
			controllerOptions: [
				{
					el: ".btn_pre",
					rewind:true,
					step: 150
				},
				{
					el: ".btn_next",
					rewind:true,
					step: -150
				},
			]
		});
		$('.flowslider_box td').hover(function(e) {
			$(".btn-pre, .btn-next").css("visibility","visible");},
			function(e) {
			$(".btn-pre, .btn-next").css("visibility","hidden");
		});
	
		   $(".hover_bac").css('display','none'); 
			$(".item").hover(function(e) {
			   $(".hover_bac").slice( "slow" );
	   		   $(this).children(".hover_bac").stop(true,false).toggle( "slow" ); 
			});
			
	});
</script>

<script type="text/javascript">
	jQuery(document).ready(function(e) {
		jQuery( ".under_bloc_slide1" ).click(function() {
			jQuery(".inside").slideUp( "slow" );
			jQuery(this).children(".inside").stop(true,false).slideToggle( "slow" );
		});
	});
</script>
<style>
@media (width:1680px){
.slider-horizontal{
	width:1317px;
}
.btn_text_space{
	margin-left:8%;
}
}
</style>

</head>

<body bgcolor="#f1f1f1">

    <?php include("includes/header.php");?>
    
   
    <!--
        <ul id="cbp-bislideshow" class="cbp-bislideshow">
            <li><img src="images/backgrounds/01.jpg" /></li>
            <li><img src="images/backgrounds/02.jpg" /></li>
            <li><img src="images/backgrounds/03.jpg" /></li>
        </ul>
      <div id="cbp-bicontrols" class="cbp-bicontrols">
          <span class="cbp-biprev"></span> 
          <span class="cbp-bipause"></span> 
          <span class="cbp-binext"></span> 
      </div>
    </div>-->
    <div class="main_new">
        <div id="wowslider-container1">
            <div class="ws_images">
                <ul>
                    <li><img src="images/backgrounds/09.jpg"/></li>
                    <li><img src="images/backgrounds/07.jpg"/></li>
                    <li><img src="images/backgrounds/08.jpg"/></li>
                     <li><img src="images/backgrounds/10.jpg"/></li>
                </ul>
            </div>
            <div class="share">
            	<a target="_blank" href="https://www.facebook.com/sharer.php?u=alpha-creatives.com/alpha-architectures.com/images/backgrounds/01.jpg">
                	<img src="engine1/shared2.jpg" width="100%"/>
                </a>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
    <?php include('includes/menu_mid.php');?>
    
        <div class="main_detial">
    	<div class="slide_menu">
    		<div class="logo_slide">
				<div class="logo_left">		        	
                    <img src="images/logo/logo_index.png" width="100%" />
                </div>
                <div class="logotext_right">
                	<img src="images/menu_slide/Urban.png" width="100%" />
                </div>	
        	</div>
            <div class="pic_menu_slide">
				<div class="flowslider_box">
                    <table>
                        <tr>
                            <td valign="middle"><a href="#" class="btn-next"></a></td>
                            <td>
                                <div id="slider" class="slider-horizontal">
                                    <?php include("includes/three_type.php");?>
                                </div>
                            </td>
                            <td valign="middle"><a href="#" class="btn-pre"></a></td>
                        </tr>
                    </table>
				</div>
 
                 
                <div class="border"></div>
                 
                <div class="button">
                 	<div class="btn_pre">
                    	<img src="images/menu_slide/previous.png" width="100%"/>
                    </div>
                    <div class="btn_text kozu">
                    	<div class="new_title tra">
                        	 We work in open design studios with collaborative team structures, allowing all staff to contribute value and enhance our strengths and design sensitivities.
                        </div>
                    	<!--<span class="btn_text_space">PRODUCTION FEATURE ALPHA</span>
                        <span class="btn_text_space">ACRO POLO HOUSE PROJECT</span>
						<span class="btn_text_space">ACRO POLO HOUSE PROJECT</span>-->
    				</div>
                    <div class="btn_next">
                    	<img src="images/menu_slide/next.png" width="100%" />
                    </div>
                </div>
			</div>
            
    	</div>
        
    	<!--<div class="location_img">
        	<img src="images/home/01.jpg" width="100%" />
        </div>-->
        <?php include("includes/footer.php");?>
	</div>
   <script>
	jQuery(document).ready(function(e) {
		jQuery('.pro').click(function(e) {
         	jQuery('.menu_text').toggle("hid");  
        });
		jQuery('.content1').click(function(e) {
         	jQuery('.menu_text').addClass("hid");  
        });
    });
	</script>
   

</body>
</html>