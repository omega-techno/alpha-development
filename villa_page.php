<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css"/>


<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<script type="text/javascript" src="engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
</head>

<body>
	<div class="content_villa kozu">
    	<div class="menu_and_text">
        	<div class="logo_text">
            	<span style="color:#666">LUXURY</span> VILLA
            </div>
            
        	<div class="menu_villa">
            	<div class="inline_menu_villa">ABOUT</div>
                <div class="inline_menu_villa">BOOK NOW</div>
                <div class="inline_menu_villa">BLOG</div>
                <div class="inline_menu_villa">AREAS</div>
                <div class="inline_menu_villa">GALLARY</div>
                <div class="inline_menu_villa">FIND US</div>
            </div>
        </div>
        
        <div class="slide_left_and_bloc_text">
        	<div class="slide_left">
            	<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
<div class="ws_images"><ul>
		<li><img src="images/03.jpg" alt="Desert" title="Desert" id="wows1_0"/></li>
	</ul></div>
</div>	
<script type="text/javascript" src="slide798x559/engine1/wowslider.js"></script>
<script type="text/javascript" src="slide798x559/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->
            </div>
            <div class="bloc_text">
            	<div class="detail_text_left">
                	VILLA <br />
                    APHRODITE
                    <div class="detail_small_text">
                    	We love that, humanism is the soul of design and enables transformative results. We view each design opportunity as an unfolding story – and the person occupying the space or using a product – as the central character. By focusing on the individual, we humanize design. As a result, people experience our work spatially, visually and emotionally.
					</div>
                </div>
                <div class="detail_text_right">
                	<div class="three_list">We love that</div>
                    <div class="three_list">soul of design</div>
                    <div class="three_list">humanism is the</div>
                    
                    
                   <div class="detail_small_text1">
                    	We love that, humanism is the soul of design and enables transformative results. We view each design opportunity as an unfolding story – and the person occupying the space or using a product – as the central character. By focusing on the individual, we humanize design. As a result, people experience our work spatially, visually and emotionally.
					</div>
                </div>
                <div class="text_bottom">
                	<div class="combo">experience</div>
                    <div class="share_social">share 
                    	<span>
                        <img src="images/follow_us/ffb.png" />
                        <img src="images/follow_us/ftw.png" />
                        </span>
                    </div>
                </div>
                <div class="btn">
                	REVERS
                </div>
            </div>
        </div>
    </div>
</body>
</html>