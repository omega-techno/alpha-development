<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" />
<title>Untitled Document</title>
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="slide/slide_full_width/engine1/style.css" />
<script type="text/javascript" src="slide/slide_full_width/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
</head>

<body>
	<div class="content_villa">
    	<div class="menu_and_text1">
        	<div class="logo_text">
            	<span style="color:#666">LUXURY</span><span style="color:#f2f2f2">VILLA</span>
            </div>
            
        	<div class="menu_villa1">
            	<div class="inline_menu_villa">ABOUT</div>
                <div class="inline_menu_villa">BOOK NOW</div>
                <div class="inline_menu_villa">BLOG</div>
                <div class="inline_menu_villa">AREAS</div>
                <div class="inline_menu_villa">GALLARY</div>
                <div class="inline_menu_villa">FIND US</div>
            </div>
        </div>
        <div class="slide_full_width">
        	<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
<div class="ws_images"><ul>
		<li><img src="images/06.jpg" alt="bg-5-1024x576" title="Luxury Villa" id="wows1_0"/></li>
	</ul></div>

</div>	
<script type="text/javascript" src="slide/slide_full_width/engine1/wowslider.js"></script>
<script type="text/javascript" src="slide/slide_full_width/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->
        </div>
        <div class="big_title kozu">
        	<span style="font-weight:bold">ABOUT</span> <span style="color:#A4A4A4">VILLA</span>
        </div>
        <div class="menu_inline_three">
        </div>
    </div>
</body>
</html>