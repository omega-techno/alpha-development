<style>
.header{
	width:100%;
	margin:auto;
	overflow:auto;
	position:relative;
	padding-bottom:1%;
	padding-top:1%;
	background-color:#000;
	z-index:2;
	display:none;
	}
.logo{
	width:2.5%;
	float:left;
	}
a{
	color:#666;
	font-size:13px;
	text-decoration:none;
	}
a:hover{
	color:#FF931E;
	}
.menu_left{
	width: 48.75%;
	float: left;
	padding-top: 0.5%;
	padding-left: 3%;
	}
.menu_left ul{
	padding:0;
	margin:0;
	text-align:left;
	}
.menu_left ul li{
	color:#aeaeae;
	display:inline;
	}
.menu_right{
	width:25%;
	float:right;
	padding-top:0.5%;
	}
.menu_right ul{
	padding: 0px;
	margin: 0px;
	text-align: right;
	padding-right: 12%;
	}
.menu_right ul li{
	color:#aeaeae;
	display:inline;
	}
.bar{
	padding-left:1%;
	padding-right:1%;
	}
</style>	
    <div class="header">
    	<div class="menu_left">
            <ul>
                <li><a href="#">valuation</a></li>
                <li class="bar">|</li>
                <li><a href="#">improvement</a></li>
                <li class="bar">|</li>
                <li><a href="#">development</a></li>
                <li class="bar">|</li>
                <li><a href="#">investment</a></li>
            </ul>
    	</div>
    	<div class="logo">
        	<img src="images/logo/logo.png" width="100%" />
        </div>
        <div class="menu_right">
            <ul>
                <li><a href="#">agency</a></li>
                <li class="bar">|</li>
                <li><a href="#">approach</a></li>
                <li class="bar">|</li>
                <li><a href="#">location</a></li>
            </ul>
    	</div>
    </div>
    
    