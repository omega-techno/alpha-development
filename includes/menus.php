<style>
.menu{
	width:100%;
	margin:auto;
	position:relative;
	overflow:auto;
	}
.all_pro{
	color: #111;
	text-align: center;
	cursor: pointer;
	width: 12%;
	margin: 1% auto;
	position:relative;
	z-index:2;
	}
.teen_box{
	width: 58%;
	overflow: auto;
	display:none;
	position: absolute;
	left: 21%;
	z-index:3;
	}
.all_box{
	width:19%;
	float:left;
	background-color:#fff;
	margin-right:0.5%;
	margin-left:0.5%;
	margin-bottom:1%;
	}
.img_box{
	width:100%;
	margin:auto;
	}
.text_box{
	text-align:center;
	font-family:"Lato",Calibri,Arial,sans-serif;
	font-size:13px;
	color:#666;
	padding-bottom:10%;
	}
.search{
	font-family:"Lato",Calibri,Arial,sans-serif;
	font-size:13px;
	color:#666;
	width: 100%;
	float:left;
	}
.type_properties{
	width:100%;
	margin:auto;
	overflow:auto;
	padding-left:0.5%;
	padding-right:0.5%;
	margin-top:2%;
	}
.all_check_box{
	float: left;
	text-align: center;
	color: #FF931E;
	font-size: 13px;
	background-color: #FFF;
	padding-top: 1.5%;
	padding-bottom: 2.2%;
	margin-right: 0.5%;
	margin-left: 0.5%;
	width: 19%;
	}
.input_search{
	width: 100%;
	overflow: auto;
	margin-bottom: 0.7%;
	}
.textsearch{
	float: left;
	width: 19%;
	margin-right: 0.5%;
	margin-left: 0.5%;
	}
.textsearch_in{
	width: 100%;
	text-align: center;
	color: #000;
	padding-bottom: 10%;
	padding-top: 10%;
	}
.send{
	float: left;
	width: 19%;
	}
.send_in{
	width: 100%;
	background-color: #FF931E;
	color: #FFF;
	font-family: "Lato",Calibri,Arial,sans-serif;
	text-align: center;
	border: medium none;
	margin-left: 3%;
	padding-bottom: 10%;
	padding-top: 10%;
	}
.properties_type{
	width:100%;
	overflow:auto;
	margin-bottom:2%;
	}
.all_select{
	width:19%;
	float:left;
	margin-right:0.5%;
	margin-left:0.5%;
	}
.all_select_in{
	width: 100%;
	padding-bottom: 10%;
	padding-top: 10%;
	text-align: center;
	color: #707070;
	}
</style>
    <div class="menu tra">
    	<div class="all_pro">
        	<img src="images/logo/1.png" width="20%" />
            <br />
            <img src="images/logo/2.png" width="100%" />
        </div>
    </div>
    <div class="teen_box">
        <div class="all_box">
        	<div class="img_box">
            	<img src="images/menu/01.png" width="100%" />
            </div>
            <div class="text_box">architecture + design</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/02.png" width="100%" />
            </div>
            <div class="text_box">interior design</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/03.png" width="100%" />
            </div>
            <div class="text_box">condominium</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/04.png" width="100%" />
            </div>
            <div class="text_box">luxury hotel</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/05.png" width="100%" />
            </div>
            <div class="text_box">commercial</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/06.png" width="100%" />
            </div>
            <div class="text_box">residential</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/07.png" width="100%" />
            </div>
            <div class="text_box">retail</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/08.png" width="100%" />
            </div>
            <div class="text_box">land</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/09.png" width="100%" />
            </div>
            <div class="text_box">borey</div>
        </div>
        <div class="all_box">
            <div class="img_box">
            	<img src="images/menu/10.png" width="100%" />
            </div>
            <div class="text_box">project management</div>
        </div>
        <div class="search">
        	<form action="" method="post">
            	<div class="type_properties">
                	
                </div>
                <div class="input_search">
                	<div class="all_check_box">
                    	properties for rent<input type="checkbox" name="checkbox" onclick="return" />
                    </div>
                    <div class="all_check_box" style="text-align:center;">
                    	for sale<input type="checkbox" name="checkbox" onclick="return" />
                    </div>
                    <div class="all_check_box">
                    	for investment<input type="checkbox" name="checkbox" onclick="return" />
                    </div>
                	<div class="textsearch">
                    	<input class="textsearch_in" placeholder="Enter your keywords" type="text" name="textsearch" />
                    </div>
                    <div class="send">
                    	<input class="send_in" type="submit" value="search" />
                    </div>
                </div>
                <div class="properties_type">
                	<div class="all_select">
                    	<select class="all_select_in">
                        	<option>property type</option>
                        	<option>condominium</option>
                        	<option>condo</option>
                        	<option>flat</option>
                        	<option>house</option>
                        	<option>land</option>
                        	<option>other</option>
                        	<option>serviced condominium</option>
                        	<option>townhouse</option>
                        	<option>villa</option>
                        </select>
                    </div>
                	<div class="all_select">
                    	<select class="all_select_in">
                        	<option>min bed</option>
                        	<option>1</option>
                        	<option>2</option>
                        	<option>3</option>
                        	<option>4</option>
                        	<option>5</option>
                        	<option>6</option>
                        	<option>7</option>
                        	<option>8</option>
                        	<option>9</option>
                        	<option>10</option>
                        </select>
                    </div>
                	<div class="all_select">
                    	<select class="all_select_in">
                        	<option>max bed</option>
                        	<option>1</option>
                        	<option>2</option>
                        	<option>3</option>
                        	<option>4</option>
                        	<option>5</option>
                        	<option>6</option>
                        	<option>7</option>
                        	<option>8</option>
                        	<option>9</option>
                        	<option>10</option>
                        </select>
                    </div>
                	<div class="all_select">
                    	<select class="all_select_in">
                        	<option>price from</option>
                        	<option>$50</option>
                        	<option>$100</option>
                        	<option>$200</option>
                        	<option>$300</option>
                        	<option>$400</option>
                        	<option>$500</option>
                        	<option>$600</option>
                        	<option>$700</option>
                        	<option>$800</option>
                        	<option>$900</option>
                        	<option>$1,000+</option>
                        </select>
                    </div>
                	<div class="all_select">
                    	<select class="all_select_in">
                        	<option>price to</option>
                        	<option>$50</option>
                        	<option>$100</option>
                        	<option>$200</option>
                        	<option>$300</option>
                        	<option>$400</option>
                        	<option>$500</option>
                        	<option>$600</option>
                        	<option>$700</option>
                        	<option>$800</option>
                        	<option>$900</option>
                        	<option>$1,000+</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </div>