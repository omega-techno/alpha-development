        <div class="content">
        	<div class="bloc_slide kozu">
            	<div class="image_white">
                    <img src="images/menu_slide/07.jpg"/>
                </div>
                    <!--<div id="wowslider-container2">
                            <div class="ws_images">
                                <ul>
                                    <li><img src="slide_small/data2/images/01.jpg" width="100%" /></li>
                                    <li><img src="slide_small/data2/images/04.jpg" width="100%" /></li>
                                </ul>
                            </div>
                            <script type="text/javascript" src="slide_small/engine2/wowslider2.js"></script>
                            <script type="text/javascript" src="slide_small/engine2/script2.js"></script>
                        </div>-->
            <div class="under_bloc_slide">
            			<div class="our_philo">OUR PHILOSOPHY</div>
                        <div class="our_philo_detail">Creative & Design is a tool we use to touch people, engage their senses and enhance their lives.</div>
                    	<!--<span class="small_text">PRODUCTION FEATURE ALPHA</span><span>PRODUCTION FEATURE ALPHA</span>-->
			</div>
            
            <div class="under_bloc_slide1">
            	WE LOVE
            	<div class="point_down">
                	<img src="images/point.png" width="100%" />
                </div>
               	<div class="inside">
                	We love that, humanism is the soul of design and enables transformative results. We view each design opportunity as an unfolding story – and the person occupying the space or using a product – as the central character. By focusing on the individual, we humanize design. As a result, people experience our work spatially, visually and emotionally.
                </div>   	
			</div>
            <div class="under_bloc_slide1">
            	WE CARE
            	<div class="point_down">
                	<img src="images/point.png" width="100%" />
                </div>
               	<div class="inside">
                	We care that, architecture is inextricably tied to use, place, client aspirations and cultural traditions.  We also believe that the greatest buildings are those that cannot be imagined anywhere else in the world.  Thus, it is our mission to provide clients around the globe with highly functional, intuitive design solutions that are sensitive to an ever changing built environment in both scale and context.  We bring design excellence to all building types.
                </div>   	
			</div>
            <div class="under_bloc_slide1">
            	WE SHARE 
            	<div class="point_down">
                	<img src="images/point.png" width="100%" />
                </div>
               	<div class="inside">
                	We share that, interior design should reflect each client’s culture and communicate their values while meeting both their functional and aesthetic goals. Our projects range from company headquarters to international hotels to interiors for public institutions. Working in conjunction with Alpha’s product designers, our interior designers often custom-design furniture and artwork that complement the character of the firm’s architecture and interiors
                </div>
                  	
			</div>
            <div class="right_size">
        	<div class="our_philo1">CEO MESSAGE</div>
            <div class="our_philo_detail1">
            	What I do is build a comfortable for life, great urbanism and spaces can inspire, influence and enhance the lives of their users and the community. Spaces for learning, for socializing, for work, contemplative spaces, and spaces to live and grow old in, must contribute to the positive qualities of life, productivity, and happiness of their occupants.<br /><br />

It puts a love, care & share in my life every days I come to work…The genius of the architecture with urbanism design is in the way it honors like Angkor era’ history while redefining the organization for the future.

            </div>
            
            <div class="our_philo2">AM GELIQUE</div>
            <div class="our_philo_detail2 tra">
            	Founder & CEO
            </div>
        	
            
        	</div>
            </div>
            <div class="bloc_icon_menu kozu">
				<div class="bloc_icon">            	
                    <div class="bloc_small_icon">
                        <img src="images/menu_slide/home.png" width="50%"/>
                        	<div class="tit_menu">home</div>
                    </div>
                    
                    
                    <div class="space_icon_menu"></div>
                    
                    <div class="bloc_small_icon">
                        <img src="images/menu_slide/menu.png" width="50%"/>
                       		<div class="tit_menu">company</div>
                    </div>
                     
                    
                    <div class="space_icon_menu"></div>
                    
                    <div class="bloc_small_icon pro">
                        <img src="images/menu_slide/book.png" width="50%"/>
                        	<div class="tit_menu">project</div>
                    </div>
               	</div>
                    
                <div class="menu_text kozu">
                	<ul>
                    	<li>architecture </li>
                        <li>interior design </li>
                        <li>landscape design </li>
                        <li>master planning </li>
                        <li>environmental branding</li>
                        <li>lighting design </li>
                        <li>experience design</li>
					</ul>
                    
                <div class="bloc_social">
                	
                    <div class="social">
                   		<a href="#"><img src="images/social/google.png" width="100%" /></a>
                    </div>
                    
					<div class="social">
                   		<a href="#"><img src="images/social/facebook.png" width="100%" /></a>
                    </div>
                    
                    <div class="social">
                   		<a href="#"><img src="images/social/twiter.png" width="100%" /></a>
                    </div>
                    
                    <div class="social">
                   		<a href="#"><img src="images/social/youtube.png" width="100%" /></a>
                    </div>                    
                </div>
			</div>
		</div>
	</div>