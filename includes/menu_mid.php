<script type="text/javascript">
$(document).ready(function (){
	$(".scroll_down").click(function() {
		$('html, body').animate({
			scrollTop: $(".slide_menu").offset().top
		}, 1000);
	});
	$( ".border_left" ).hover(function() {
		$(".sub_menu").slideUp( "slow" );
		$(this).children(".sub_menu").stop(true,false).slideToggle( "slow" );
	});
});
</script>
<style>
.menu_mid{
	width:100%;
	position:absolute;
	bottom:0;
	z-index:2;
	}
.in_mid_menu{
	width:20%;
	float:left;
	background-color:#fff;
	}
.border_left{
	width:100%
	cursor:pointer;
	padding:4% 0%;
	}
.border_left_in{
	width:100%;
	cursor:pointer;
	border-left:1px solid #D0D0C0;
	padding-left:2%;
	overflow:auto;
	}
.sub_menu{
	display:none;
	position: absolute;
	bottom: 100%;
	z-index: 23;
	width: 20%;
	}
.img_sub{
	width:100%;
	padding:2%;
	background-color:#fff;
	}
.sub_top{
	padding:5%;
	font-family:"Lato",Calibri,Arial,sans-serif;
	font-size:13px;
	color:#D3C480;
	margin-bottom:1%;
	background-color:#000000;
	}
.sub_bottom{
	padding:5%;
	font-family:"Lato",Calibri,Arial,sans-serif;
	font-size:13px;
	background-color:#fff;
	margin-bottom:1%;
	}
.logo_in_mid{
	width:8.5%;
	}
.text_menu_mid{
	font-family: "Lato",Calibri,Arial,sans-serif;
	font-size: 13px;
	color: #4F4F4F;
	text-align: left;
	margin: 1% 0%;
	padding-left: 3%;
	font-weight: bold;
	}
.text_menu_mid_down{
	color: #FF931E;
	font-family: "Lato",Calibri,Arial,sans-serif;
	font-size: 13px;
	text-align: left;
	margin: 1% 0%;
	padding-left: 3%;
	font-weight: bold;
	}
.scroll_down{
	width:100%;
	background-color:#000;
	float:left;
	opacity:0.4;
	}
.scroll_text{
	font-family:"Lato",Calibri,Arial,sans-serif;
	font-size:13px;
	color:#FFF;
	text-align:center;
	cursor:pointer;
	width:10%;
	margin:auto;
	padding: 0.65%;
	}
.simble{
	float:right;
	margin-right:5%;
	}
.simble_down{
	float:right;
	margin-top:5.5%;
	}
.mid_bar{
	padding-left:4%;
	padding-right:4%;
	}
</style>
<div class="menu_mid">
    <!--<div class="in_mid_menu">
    	<div class="border_left">
            <div class="border_left_in" style="border:none;">
                <div class="text_menu_mid">CORPORATE COMPANIES<div class="simble"><img src="images/logo/simble.png" /></div></div>
                <div class="text_menu_mid_down">Alpha Realty & Development Co., Ltd.</div>
            </div>
            <div class="sub_menu one">
            	<div class="img_sub">
                	<img src="images/home/1.jpg" width="100%" />
                </div>
                <div class="sub_bottom">
                    <a href="">alpha business travel</a>
                </div>
                <div class="sub_bottom">
                    <a href="">alpha architecture + design</a>
                </div>
                <div class="sub_bottom">
                    <a href="">alpha creative + advertising</a>
                </div>
            </div>
    	</div>
    </div>
	<div class="in_mid_menu">
    	<div class="border_left">
        	<div class="border_left_in">
                <div class="text_menu_mid">ALPHA SERVICES<div class="simble"><img src="images/logo/simble.png" /></div></div>
                <div class="text_menu_mid_down">New Launches</div>
            </div>
            <div class="sub_menu one">
            	<div class="img_sub">
                	<img src="images/home/1.jpg" width="100%" />
                </div>
                <div class="sub_bottom">
                    <a href="">property for rent </a>
                </div>
                <div class="sub_bottom">
                    <a href="">property for sale</a>
                </div>
                <div class="sub_bottom">
                    <a href="">property for investment</a>
                </div>
                <div class="sub_bottom">
                    <a href="">economic news</a>
                </div>
            </div>
    	</div>
    </div>
	<div class="in_mid_menu">
    	<div class="border_left">
        	<div class="border_left_in">
                <div class="text_menu_mid">ALPHA APPROACH<div class="simble"><img src="images/logo/simble.png" /></div></div>
                <div class="text_menu_mid_down">Properties Collection</div>
            </div>
            <div class="sub_menu one">
            	<div class="img_sub">
                	<img src="images/home/1.jpg" width="100%" />
                </div>
                <div class="sub_bottom">
                    <a href="">condominium</a>
                </div>
                <div class="sub_bottom">
                    <a href="">industrial </a>
                </div>
                <div class="sub_bottom">
                    <a href="">landed</a>
                </div>
                <div class="sub_bottom">
                    <a href="">office</a>
                </div>
                <div class="sub_bottom">
                    <a href="">shop </a>
                </div>
                <div class="sub_bottom">
                    <a href="">residential</a>
                </div>
                <div class="sub_bottom">
                    <a href="">commercial</a>
                </div>
                <div class="sub_bottom">
                    <a href="">institutional / public space</a>
                </div>
                <div class="sub_bottom">
                    <a href="">hospitality</a>
                </div>
            </div>
    	</div>
    </div>
	<div class="in_mid_menu">
	    <div class="border_left">
        	<div class="border_left_in">
                <div class="text_menu_mid">ABOUT ALPHA<div class="simble"><img src="images/logo/simble.png" /></div></div>
                <div class="text_menu_mid_down">The Company</div>
            </div>
            <div class="sub_menu one">
            	<div class="img_sub">
                	<img src="images/home/1.jpg" width="100%" />
                </div>
                <div class="sub_bottom">
                    <a href="">our team</a>
                </div>
                <div class="sub_bottom">
                    <a href="">approaches </a>
                </div>
                <div class="sub_bottom">
                    <a href="">downloads</a>
                </div>
                <div class="sub_bottom">
                    <a href="">pdpa (personal data protection act 2016)</a>
                </div>
            </div>
    	</div>
    </div>
	<div class="in_mid_menu">
    	<div class="border_left">
        	<div class="border_left_in">
                <div class="text_menu_mid">PROJECT DESIGN<div class="simble"><img src="images/logo/simble.png" /></div></div>
                <div class="text_menu_mid_down">Design, Develop, & Create</div>
            </div>
            <div class="sub_menu one">
            	<div class="img_sub">
                	<img src="images/home/1.jpg" width="100%" />
                </div>
                <div class="sub_bottom">
                    <a href="">architecture</a>
                </div>
                <div class="sub_bottom">
                    <a href="">interior design</a>
                </div>
                <div class="sub_bottom">
                    <a href="">landscape design</a>
                </div>
                <div class="sub_bottom">
                    <a href="">master planning</a>
                </div>
                <div class="sub_bottom">
                    <a href="">environmental branding</a>
                </div>
                <div class="sub_bottom">
                    <a href="">lighting design</a>
                </div>
                <div class="sub_bottom">
                    <a href="">experience design</a>
                </div>
            </div>
        </div>
    </div>-->
    <div class="scroll_down">
    	<div class="scroll_text">s c r o l l &nbsp; d o w n<div class="simble_down"><img src="images/logo/simble_down.png" /></div></div>
    </div>
</div>